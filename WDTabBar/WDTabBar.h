//
//  WDTabBar.h
//  WDTabBar
//
//  Created by Matt Brandt on 2/21/14.
//  Copyright (c) 2014 Matt Brandt. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "WDTabBarButton.h"

@protocol WDTabBarDelegate;

@interface WDTabBar : NSView <WDTabBarButtonDelegate>

@property (nonatomic, strong) NSMutableArray *tabs;
@property (nonatomic, strong) NSMutableArray *buttonLayouts;
@property (nonatomic, strong) id<WDTabBarDelegate> delegate;

- (void)addTabWithName: (NSString *)name;
- (NSString *)nameOfTabAtIndex: (NSInteger)index;
- (IBAction)selectLastTab: sender;

@end

@protocol WDTabBarDelegate <NSObject>

@optional
- (BOOL)tabBar: (WDTabBar *)tabBar shouldSelectTabAtIndex: (NSInteger)index;
- (void)tabBar: (WDTabBar *)tabBar didSelectTabAtIndex: (NSInteger)index;

- (BOOL)tabBar:(WDTabBar *)tabBar shouldRenameTabAtIndex:(NSInteger)index;
- (void)tabBar:(WDTabBar *)tabBar didRenameTabAtIndex:(NSInteger)index;

@end
