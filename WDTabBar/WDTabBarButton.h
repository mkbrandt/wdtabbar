//
//  WDTabBarButton.h
//  WDTabBar
//
//  Created by Matt Brandt on 2/21/14.
//  Copyright (c) 2014 Matt Brandt. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@protocol WDTabBarButtonDelegate;

@interface WDTabBarButton : NSView

@property (nonatomic, assign) BOOL selected;
@property (nonatomic, strong) NSTextField *label;
@property (nonatomic, strong) id<WDTabBarButtonDelegate> delegate;

@end

@protocol WDTabBarButtonDelegate <NSObject>

@required
- (BOOL)shouldBeginEditing: (WDTabBarButton *)button;
- (void)tabDidBeginEditing: (WDTabBarButton *)button;
- (void)tabDidEndEditing: (WDTabBarButton *)button;
- (BOOL)shouldSelect: (WDTabBarButton *)button;
- (void)tabSelected: (WDTabBarButton *)button;

@end

