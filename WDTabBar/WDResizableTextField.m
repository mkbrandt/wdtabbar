//
//  MBTextField.m
//  WDTabBar
//
//  Created by Matt Brandt on 2/21/14.
//  Copyright (c) 2014 Matt Brandt. All rights reserved.
//

#import "WDResizableTextField.h"

@implementation WDResizableTextField

- (id)initWithFrame:(NSRect)frameRect
{
    self = [super initWithFrame: frameRect];
    if( self )
    {
        self.delegate = self;
    }
    return self;
}

- (NSSize)intrinsicContentSize
{
    NSString *text = self.stringValue;
    NSSize size = [text sizeWithAttributes: @{NSFontAttributeName: [self font]}];
    
    size.width += 8;
    return size;
}

- (void)controlTextDidChange:(NSNotification *)obj
{
    [self invalidateIntrinsicContentSize];
}

@end
