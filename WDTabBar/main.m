//
//  main.m
//  WDTabBar
//
//  Created by Matt Brandt on 2/26/14.
//  Copyright (c) 2014 Matt Brandt. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[])
{
    return NSApplicationMain(argc, argv);
}
