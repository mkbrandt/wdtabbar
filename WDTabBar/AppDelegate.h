//
//  AppDelegate.h
//  MBTabBar
//
//  Created by Matt Brandt on 2/21/14.
//  Copyright (c) 2014 Matt Brandt. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "WDTabBar.h"

@interface AppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;
@property (assign) IBOutlet WDTabBar *tabBar;

@end
