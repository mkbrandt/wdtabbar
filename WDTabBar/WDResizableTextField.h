//
//  MBTextField.h
//  WDTabBar
//
//  Created by Matt Brandt on 2/21/14.
//  Copyright (c) 2014 Matt Brandt. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface WDResizableTextField : NSTextField <NSTextFieldDelegate>

@end
