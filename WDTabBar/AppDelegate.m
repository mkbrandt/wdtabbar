//
//  AppDelegate.m
//  MBTabBar
//
//  Created by Matt Brandt on 2/21/14.
//  Copyright (c) 2014 Matt Brandt. All rights reserved.
//

#import "AppDelegate.h"

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    [self.tabBar addTabWithName: @"Page 1"];
    [self.tabBar addTabWithName: @"Page 2"];
    [self.tabBar addTabWithName: @"Page 3"];
    [self.tabBar addTabWithName: @"Page 4"];
}

@end
