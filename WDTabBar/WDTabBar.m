//
//  WDTabBar.m
//  WDTabBar
//
//  Created by Matt Brandt on 2/21/14.
//  Copyright (c) 2014 Matt Brandt. All rights reserved.
//

#import "WDTabBar.h"

@implementation WDTabBar
{
    NSInteger selectedTab;
}

- (id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.tabs = [NSMutableArray array];
        self.buttonLayouts = [NSMutableArray array];
    }
    return self;
}

- (void)drawRect:(NSRect)dirtyRect
{
    [[NSColor grayColor] set];
    NSRectFill(self.bounds);
}

- (void)reJiggerConstraints
{
    [self removeConstraints: self.buttonLayouts];
    [self.buttonLayouts removeAllObjects];
    
    if( self.tabs.count > 0 )
    {
        WDTabBarButton *tab = self.tabs[0];

        NSLayoutConstraint *constraint = [NSLayoutConstraint constraintWithItem: tab
                                                         attribute: NSLayoutAttributeLeft
                                                         relatedBy: NSLayoutRelationEqual
                                                            toItem: self
                                                         attribute: NSLayoutAttributeLeft
                                                        multiplier: 1
                                                          constant: 0];
        [self addConstraint: constraint];
        [self.buttonLayouts addObject: constraint];
        
        constraint = [NSLayoutConstraint constraintWithItem: tab
                                                         attribute: NSLayoutAttributeCenterY
                                                         relatedBy: NSLayoutRelationEqual
                                                            toItem: self
                                                         attribute: NSLayoutAttributeCenterY
                                                        multiplier: 1
                                                          constant: 0];
        [self addConstraint: constraint];
        [self.buttonLayouts addObject: constraint];
    }
    
    for( int i = 1; i < self.tabs.count; ++i )
    {
        WDTabBarButton *tab = self.tabs[i];
        WDTabBarButton *prev = self.tabs[i - 1];
        
        NSLayoutConstraint *constraint = [NSLayoutConstraint constraintWithItem: tab
                                                         attribute: NSLayoutAttributeLeft
                                                         relatedBy: NSLayoutRelationEqual
                                                            toItem: prev
                                                         attribute: NSLayoutAttributeRight
                                                        multiplier: 1
                                                          constant: -1];
        [self addConstraint: constraint];
        [self.buttonLayouts addObject: constraint];
        
        
        constraint = [NSLayoutConstraint constraintWithItem: tab
                                                         attribute: NSLayoutAttributeCenterY
                                                         relatedBy: NSLayoutRelationEqual
                                                            toItem: self
                                                         attribute: NSLayoutAttributeCenterY
                                                        multiplier: 1
                                                          constant: 0];
        [self addConstraint: constraint];
        [self.buttonLayouts addObject: constraint];
    }
}

- (void)addTabWithName:(NSString *)name
{
    WDTabBarButton *tab = [[WDTabBarButton alloc] initWithFrame: NSMakeRect(0, 0, 80, 30)];
    
    tab.label.stringValue = name;
    tab.delegate = self;
    [self.tabs addObject: tab];
    [self addSubview: tab];
    [self reJiggerConstraints];
}

- (NSString *)nameOfTabAtIndex:(NSInteger)index
{
    WDTabBarButton *tab = self.tabs[index];
    
    return tab.label.stringValue;
}

- (IBAction)selectLastTab: (id)sender
{
    WDTabBarButton *tab = [self.tabs lastObject];
    
    tab.selected = YES;
}

- (void)tabSelected:(WDTabBarButton *)button
{
    NSInteger tab = [self.tabs indexOfObject: button];
    
    for( int i = 0; i < self.tabs.count; ++i )
    {
        if( i != tab )
        {
            WDTabBarButton *tbb = self.tabs[i];
            
            tbb.selected = NO;
        }
    }
    
    NSLog(@"Select tab %ld\n", tab);
    if( [self.delegate respondsToSelector: @selector(tabBar:didSelectTabAtIndex:)] )
    {
        [self.delegate tabBar: self didSelectTabAtIndex: tab];
    }
}

- (BOOL)shouldBeginEditing:(WDTabBarButton *)button
{
    if( [self.delegate respondsToSelector: @selector(tabBar:shouldRenameTabAtIndex:)] )
    {
        NSInteger index = [self.tabs indexOfObject: button];
        
        return [self.delegate tabBar: self shouldRenameTabAtIndex: index];
    }
    return YES;
}

- (void)tabDidBeginEditing:(WDTabBarButton *)button
{
    NSInteger tab = [self.tabs indexOfObject: button];
    
    NSLog(@"Begin editing tab %ld\n", tab);
}

- (void)tabDidEndEditing:(WDTabBarButton *)button
{
    NSInteger tab = [self.tabs indexOfObject: button];
    
    NSLog(@"End editing tab %ld\n", tab);
    if( [self.delegate respondsToSelector: @selector(tabBar:didRenameTabAtIndex:)] )
    {
        NSInteger index = [self.tabs indexOfObject: button];

        [self.delegate tabBar: self didRenameTabAtIndex: index];
    }
}

- (BOOL)shouldSelect:(WDTabBarButton *)button
{
    if( [self.delegate respondsToSelector: @selector(tabBar:shouldSelectTabAtIndex:)] )
    {
        NSInteger index = [self.tabs indexOfObject: button];
        
        return [self.delegate tabBar: self shouldSelectTabAtIndex: index];
    }
    return YES;
}

@end
