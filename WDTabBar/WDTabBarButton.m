//
//  WDTabBarButton.m
//  WDTabBar
//
//  Created by Matt Brandt on 2/21/14.
//  Copyright (c) 2014 Matt Brandt. All rights reserved.
//

#import "WDTabBarButton.h"
#import "WDResizableTextField.h"

@implementation WDTabBarButton

- (id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        NSRect textFrame = NSInsetRect(self.bounds, 10, 5);
        self.label = [[WDResizableTextField alloc] initWithFrame: textFrame];
        self.label.stringValue = @"Untitled";
        [self.label setEditable: NO];
        [self.label setEnabled: YES];
        [self.label setBordered: NO];
        [self.label setDrawsBackground: NO];
        [self.label setAction: @selector(endEditing:)];
        [self.label setTarget: self];
        [self.label setAlignment: NSCenterTextAlignment];
        self.label.translatesAutoresizingMaskIntoConstraints = NO;
        self.translatesAutoresizingMaskIntoConstraints = NO;
        [self addConstraint: [NSLayoutConstraint constraintWithItem: self
                                                          attribute: NSLayoutAttributeWidth
                                                          relatedBy: NSLayoutRelationEqual
                                                             toItem: self.label
                                                          attribute: NSLayoutAttributeWidth
                                                         multiplier: 1
                                                           constant: 40]];
        [self addConstraint: [NSLayoutConstraint constraintWithItem: self.label
                                                          attribute: NSLayoutAttributeCenterX
                                                          relatedBy: NSLayoutRelationEqual
                                                             toItem: self
                                                          attribute: NSLayoutAttributeCenterX
                                                         multiplier: 1
                                                           constant: 0]];
        [self addConstraint: [NSLayoutConstraint constraintWithItem: self.label
                                                          attribute: NSLayoutAttributeCenterY
                                                          relatedBy: NSLayoutRelationEqual
                                                             toItem: self
                                                          attribute: NSLayoutAttributeCenterY
                                                         multiplier: 1
                                                           constant: 0]];
        [self addSubview: self.label];
    }
    return self;
}

- (IBAction)checkSize:(id)sender
{
    [self setNeedsUpdateConstraints: YES];
}

- (IBAction)endEditing: (id)sender
{
    if( self.label.isEditable )
    {
        [self.label setEditable: NO];
        [self.delegate tabDidEndEditing: self];
    }
}

- (NSSize)intrinsicContentSize
{
    NSString *text = self.label.stringValue;
    NSSize size = [text sizeWithAttributes: @{}];
    
    size.height += 20;
    size.width += 40;
    return size;
}

- (void)drawRect:(NSRect)dirtyRect
{
    NSGradient *buttonFill;
    
    if( self.selected )
        buttonFill = [[NSGradient alloc] initWithStartingColor: [NSColor whiteColor] endingColor: [NSColor lightGrayColor]];
    else
        buttonFill = [[NSGradient alloc] initWithStartingColor: [NSColor grayColor] endingColor: [NSColor lightGrayColor]];
    
    [buttonFill drawInRect: self.bounds angle: 90];
    [[NSColor blackColor] set];
    [NSBezierPath strokeRect: NSInsetRect(self.bounds, 1, 1)];
}

- (void)setSelected:(BOOL)selected
{
    _selected = selected;
    [self setNeedsDisplay: YES];
}

- (void)mouseDown:(NSEvent *)theEvent
{
    if( theEvent.clickCount == 2 )
    {
        if( [self.delegate shouldBeginEditing: self] )
        {
            [self.label setEditable: YES];
            [self.label selectText: self];
            [self.delegate tabDidBeginEditing: self];
        }
    }
    else
    {
        if( [self.delegate shouldSelect: self] )
        {
            self.selected = YES;
            [self.delegate tabSelected: self];
        }
    }
}

@end
